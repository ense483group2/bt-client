package com.example.ahmed.tutorial;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class sendMessage extends AppCompatActivity {
    boolean color;
    String readMessage;
    Button sendbtn,disconnectbtn;
    TextView lightswitch;
    EditText message;
    String address = null;
    ConnectedThread BTconnection;
    private ProgressDialog progress;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("bfb7d366-c8ba-11e7-abc4-cec278b6b50a");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_send_message);
            //receive the address of the bluetooth device
            Intent newint = getIntent();
            address = newint.getStringExtra(DeviceList.EXTRA_ADDRESS);
            setContentView(R.layout.activity_send_message);
            //call the widgets
            disconnectbtn = findViewById(R.id.disconnect);
            sendbtn = findViewById(R.id.send);
            message = findViewById(R.id.editText);
            lightswitch = findViewById(R.id.light);
            sendbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        send();      //method to send
                    } catch (Error e) {
                        msg("Error" + e.toString());
                    }
                }
            });
            disconnectbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disconnect();      //method to disconnect
                }
            });
            new ConnectBT().execute();
        }
        catch (Error e) {
            msg(e.toString());
        }
    }

    public void onUpdate(){
        if(color){
            lightswitch.setBackgroundColor(getResources().getColor(R.color.Yellow));
        }
        else{
            if(!color){
                lightswitch.setBackgroundColor(getResources().getColor(R.color.black));
            }
        }
    }
    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected
        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(sendMessage.this, "Connecting...", "Please wait!!!");  //show a progress dialog
        }
        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice device = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = device.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);
            if (!ConnectSuccess)
            {
                msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
                finish();
            }
            else
            {
                msg("Thread Start");
                BTconnection = new ConnectedThread(btSocket);
                BTconnection.start();
                isBtConnected = true;
            }
            progress.dismiss();
        }
    }
    private class ConnectedThread extends Thread {
        private final BluetoothSocket BTSocket;
        private final InputStream IStream;
        private final OutputStream OStream;
        private byte[] mmBuffer; // mmBuffer store for the stream
        public ConnectedThread(BluetoothSocket socket) {
            BTSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                msg("Error: " + e.toString());
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                msg("Error: " + e.toString());
            }
            IStream = tmpIn;
            OStream = tmpOut;
        }
        public void run() {
            mmBuffer = new byte[1024];
            int numBytes; // bytes returned from read()
            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                try {
                    // Read from the InputStream.
                    numBytes = IStream.read(mmBuffer);
                    readMessage = new String(mmBuffer, 0, numBytes);
                    if(readMessage.equals("on")){
                        color = true;
                    }
                    else{
                        if(readMessage.equals("off")){
                           color = false;
                        }
                    }
                    runOnUiThread(new Runnable()  {
                        @Override
                        public void run(){
                            onUpdate();
                        }
                    });
                } catch (IOException e) {
                    msg("Error: " + e.toString());
                    break;
                }
            }
        }
        // Call this from the main activity to send data to the remote device.
        public void write(byte[] bytes) {
            try {
                OStream.write(bytes);

            } catch (IOException e) {
                msg("Write not working");
            }
        }
        // Call this method from the main activity to shut down the connection.
        public void cancel() {
            try {
                BTSocket.close();
            } catch (IOException e) {
                msg("Error: " + e.toString());
            }
        }
    }
    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s, Toast.LENGTH_LONG).show();
    }
    public void send()
    {
        if (btSocket!=null)
        {
            try
            {
                message =findViewById(R.id.editText);
                String messageToBeSent =  message.getText().toString();
                msg("Message:" + messageToBeSent);
                //btSocket.getOutputStream().write(messageToBeSent.getBytes());
                BTconnection.write(messageToBeSent.getBytes());
            }
            catch (Error e)
            {
                msg("Error: " + e.toString());
            }
        }
    }
    public void disconnect(){
        if (btSocket!=null) //If the btSocket is busy
        {
            try
            {
                BTconnection.cancel(); //close connection
                msg("Connection Closed");
            }
            catch (Error e)
            {  msg("Error: " + e.toString());}
        }
        finish(); //return to the first layout
    }
}
